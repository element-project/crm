<?php

namespace Empu\ElementCrm\Models;

use Model;
use October\Rain\Database\Builder;

/**
 * PipelineStage Model
 */
class PipelineStage extends Model
{
    const INITIAL_STAGE = 0;

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string table associated with the model
     */
    public $table = 'empu_elcrm_pipeline_stages';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = ['label', 'description', 'sequence', 'is_closing_stage'];

    /**
     * @var array rules for validation
     */
    public $rules = [
        'label' => 'required',
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array hasOne and other relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeCreate()
    {
        if (is_null($this->sequence)) {
            $this->sequence = self::nextSequence();
        }
    }

    public static function nextSequence()
    {
        return self::query()->max('sequence') + 1;
    }

    public function scopeGetFirstStage(Builder $builder): ?PipelineStage
    {
        return $builder->applySequence(self::INITIAL_STAGE)->first();
    }

    public function scopeApplySequence(Builder $builder, int $stage): Builder
    {
        return $builder->where($this->qualifyColumn('sequence'), $stage);
    }
}
