<?php

namespace Empu\ElementCrm\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreatePipelineStagesTable Migration
 */
class CreatePipelineStagesTable extends Migration
{
    public function up()
    {
        Schema::create('empu_elcrm_pipeline_stages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->string('description')->nullable();
            $table->unsignedTinyInteger('sequence')->default(1);
            $table->boolean('is_closing_stage')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('empu_elcrm_pipeline_stages');
    }
}
